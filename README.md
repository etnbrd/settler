# Cellar

Cellar is a set of scripts to backup and setup machines.
It is roughly composed of 4 parts :
- Credentials (a private repository)
- [Sodium](https://gitlab.com/etnbrd/sodium)
- [Chloride](https://gitlab.com/etnbrd/chloride)
- [Settler](https://gitlab.com/etnbrd/settler)

Cellar is like a dotfile manager but for whole machines, using [SaltStack](https://saltstack.com/) (Sodium + Chloride = Salt).

## The story

I used to have a dotfile repo, like everybody else, with clever symlinks. I even implemented a filter mechanism to deploy dotfiles differently on different machines.
At the same time, I use Salt to provision and maintain my personal machines (in a masterless manner).
Salt is way more powerful than what I could ever implement in my dotfiles scripts.
So I switched from bash scripts to salt states for my dotfiles.
Here is the result.

## Example

You can see [this script](https://gitlab.com/etnbrd/settler/blob/master/deploy.sh) for a complete example of deploying the state on a base machine.

## Settler

A set of small scripts to deploy machines with Chloride + Sodium.

`build.sh` and `start.sh` build and start Docker images/containers with the `Dockerfile`.

`pre-local.sh`, `pre-remote.sh` and `deploy.sh` prepare real machines to apply chloride/sodium states.

## examples

To deploy on a remote host :
```
cd /home/<user>/salt/settler
./pre-remote.sh "<remote host>" "<user>"
ssh root@<remote host>
./deploy.sh "<user>" "<machine>" "<sodium>" "<credentials>"
```

To deploy from a local media :
```
cd <local media>/salt/settler
./pre-local.sh "<user>"
arch-chroot /mnt
cd /root
./deploy.sh "<user>" "<machine>" "<sodium>" "<credentials>"
```


## Full local script to deploy a machine

```
loadkeys fr
```

### Prepare the partition
```
lsblk
# partition
cgdisk /dev/sd?
# format
mkfs.btrfs /dev/partition -L arch-dev
# mount
mount /dev/partition /mnt
```

### Connect to the network
plug an internet connection (ethernet, usb-tethering ...)
`systemctl start dhcpcd`

### Prepare the base system
```
# install the base
pacstrap /mnt base base-devel btrfs-progs git
# fstab
genfstab -p -U /mnt >> /mnt/etc/fstab
# Prepare deployement
cd /run/archiso/bootmnt/cellar/settlers
./pre-local.sh
# chroot
arch-chroot /mnt
```

### Configure the base
```
# Initramfs
mkinitcpio -p linux
# Root password
passwd
# Locale
nano /etc/locale.gen
# uncomment
# en_US.UTF-8 UTF-8
# fr_FR.UTF-8 UTF-8
locale-gen
# LANG=en_US.UTF-8
# LC_MONETARY=fr_FR.UTF-8
# LC_MEASUREMENT=fr_FR.UTF-8
# LC_PAPER=fr_FR.UTF-8
# LC_TELEPHONE=fr_FR.UTF-8
```

### Deploy
```
./deploy.sh <user> <machine> <sodium> <credentials>
# reboot, then reapply state with locales and dbus available.
sudo salt-call --local state.highstate
```

## Maintenance
To update a machine, or to take modified states into account, simply reaply states.
```
sudo salt-call --local -l all state.highstate
```
