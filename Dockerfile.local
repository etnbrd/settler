#
# dev env - Dockerfile for the base image
# by etnbrd - etn@etnbrd.com
#

from nfnty/arch-mini
maintainer etnbrd

# Update base system
run pacman-key --populate archlinux; pacman-key --refresh-keys;
run pacman -Suy --noconfirm;
run pacman -S --noconfirm base-devel salt git openssh;

# Bootstrap base user
arg user
run useradd -m ${user}
user ${user}
workdir /home/${user}
run mkdir -p /home/${user}/.cellar/{states,credentials};

# Retrieve full credentials
arg credentials
workdir /home/${user}/.cellar
copy ${credentials} credentials
# run chmod 600 credentials/ssh/id_rsa; #TODO fix this

# Retrieve states
arg sodium
arg chloride
workdir /home/${user}/.cellar/salt
copy ${sodium} sodium
copy ${chloride} chloride

# Apply minion configuration, machine grains, then states
arg machine
user root
run ln -sf /home/${user}/.cellar/salt/sodium/states/base/minion /etc/salt/minion; \
    ln -sf /home/${user}/.cellar/salt/sodium/machines/${machine}.yml /etc/salt/grains; \
    salt-call --local state.highstate;

# Cleaning credentials
run rm -rf /home/${user}/.cellar/credentials

# Cleaning image
# run pacman -Rns salt; # Salt is not needed in docker images, and it brings python3 with it.
run yes | pacman -Scc; # Remove all package cache

# Finishing
user ${user}
workdir /home/${user}
