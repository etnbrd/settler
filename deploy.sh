#! /bin/sh

#
# Deploy sodium/chloride to a minimal archlinux setup
#

Bold='\033[1;32m';
White='\033[1;37m';
Reset='\033[0m';

user=$1
machine=$2
sodium=$3
credentials=$4

cellar_root="/home/${user}/.cellar"
salt_root="/home/${user}/.cellar/salt"
sodium_root="/home/${user}/.cellar/salt/sodium"
states_root="/home/${user}/.cellar/salt/sodium/states"
chloride_root="/home/${user}/.cellar/salt/chloride"
credentials_root="/home/${user}/.cellar/credentials"

echo -e "${Bold}>>${White} Deploying ${Bold}${user}${White}@${Bold}${machine}${Reset}";

echo -n "Passphrase : ";
read -s passphrase;
echo;

# Update base system
echo -e "${Bold}>>${White} Update base system${Reset}";
pacman-key --populate archlinux; pacman-key --refresh-keys;
pacman -Suy --noconfirm;
pacman -S --noconfirm base-devel salt git openssh gnupg ca-certificates-utils;
./aur.sh git-remote-gcrypt

# Patch git-remote-gcrypt
echo -e "${Bold}>>${White} Patch git-remote-gcrypt${Reset}";
patch -N /usr/bin/git-remote-gcrypt git-remote-gcrypt.patch

# Bootstrap base user
echo -e "${Bold}>>${White} Bootstrap base user${Reset}";
useradd -m ${user}
su - ${user} -c "mkdir -p ${salt_root}";
su - ${user} -c "mkdir -p ${credentials_root}";

# Bootstrap minimal credentials
echo -e "${Bold}>>${White} Bootstrap minimal credentials${Reset}";
cd /home/${user};
su - ${user} -c "mkdir -p precred";
cp -r /root/ssh precred/;
cp -r /root/pgp precred/;
chown -R ${user}:${user} precred;
chmod -R 600 precred/ssh/id_rsa;

su - ${user} -c "ln -s /home/${user}/precred/ssh /home/${user}/.ssh";
su - ${user} -c "gpg --import --batch --no-tty --passphrase ${passphrase} /home/${user}/precred/pgp/private.pgp";

# Retrieve full credentials
echo -e "${Bold}>>${White} Retrieve full credentials${Reset}";
# cd ${cellar_root}
rm /home/${user}/precred/ssh/known_hosts;
su - ${user} -c "ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts";
su - ${user} -c "git clone --config gcrypt.gpg-args=\"--batch --pinentry-mode loopback --passphrase ${passphrase}\" gcrypt::${credentials} ${credentials_root}";
chmod 600 credentials/ssh/id_rsa; #TODO fix this

# Retrieve states
echo -e "${Bold}>>${White} Retrieve states${Reset}";
# cd ${salt_root}
su - ${user} -c "git clone git@gitlab.com:etnbrd/chloride.git ${chloride_root}";
su - ${user} -c "git clone ${sodium} ${sodium_root}";

# Apply minion configuration, machine grains, then state
echo -e "${Bold}>>${White} Apply states${Reset}";
ln -sf ${sodium_root}/states/base/minion /etc/salt/minion;
ln -sf ${sodium_root}/machines/${machine}.yml /etc/salt/grains;
salt-call --local state.highstate;

# Cleaning precred
echo -e "${Bold}>>${White} Cleaning precred${Reset}";
rm -rf /home/${user}/precred
rm -rf /root/pgp /root/ssh /root/deploy.sh /root/aur.sh
