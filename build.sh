#!/usr/bin/env bash
set -e # Exit on any failed command

acbuildend () {
    export EXIT=$?;
    acbuild --debug end && exit $EXIT;
}

#
# Build an aci image with sodium/chloride configured as the machine given in argument
#
# example
# ./build.sh "machine-name" "etn" "git@gitlab.com:etnbrd/sodium.git" "git@gitlab.com:etnbrd/credentials.git"

Bold='\033[1;32m';
Reset='\033[0m';

name=$1
user=$2
sodium=$3
credentials=$4
debug="--debug";


echo -e "${Bold}>>${Reset} Building ${Bold}${user}${Reset}/${Bold}${name}${Reset}";

# echo -n "Passphrase : ";
# read -s passphrase;
# echo;

# Begin build
acbuild ${debug} begin docker://nfnty/arch-mini
trap acbuildend EXIT #
# acbuild ${debug} dep add docker://nfnty/arch-mini
acbuild ${debug} set-name ${user}/${name};

# Update base system
echo -e "${Bold}>>${White} Update base system${Reset}";
# acbuild ${debug} set-user root
acbuild ${debug} run -- rm -rf .gnupg
acbuild ${debug} run -- pacman-key --populate archlinux; pacman-key --refresh-keys;
acbuild ${debug} run -- pacman -Suy --noconfirm;
acbuild ${debug} run -- pacman -S --noconfirm base-devel salt git openssh gnupg;
acbuild ${debug} copy aur.sh
acbuild ${debug} run -- ./aur.sh git-remote-gcrypt; \
                        rm aur.sh;

# Patch git-remote-gcrypt
echo -e "${Bold}>>${White} Patch git-remote-gcryp${Reset}";
acbuild ${debug} copy git-remote-gcrypt.patch
acbuild ${debug} run -- patch /usr/bin/git-remote-gcrypt git-remote-gcrypt.patch; \
                        rm git-remote-gcrypt.patch;

# Bootstrap base user
echo -e "${Bold}>>${White} Bootstrap base user${Reset}";
acbuild ${debug} run useradd -m ${user}
acbuild ${debug} set-user ${user}

# Bootstrap minimal credential
echo -e "${Bold}>>${White} Bootstrap minimal credential${Reset}";
acbuild ${debug} run mkdir -p /home/${user}/precred;
acbuild ${debug} set-working-directory /home/${user}/precred;
acbuild ${debug} copy /home/${user}/salt/credentials/{pgp,ssh}
acbuild ${debug} run chown -R ${user}:${user} .;
acbuild ${debug} run chmod 600 ssh/id_rsa;
acbuild ${debug} run ln -s /home/${user}/precred/ssh .ssh;
acbuild ${debug} run gpg --import --batch --no-tty --passphrase '${passphrase}' precred/pgp;

# Retrieve full credentials
echo -e "${Bold}>>${White} Retrieve full credential${Reset}";
acbuild ${debug} run mkdir -p /home/${user}/salt/{states,credentials};
acbuild ${debug} set-working-directory /home/${user}/salt
acbuild ${debug} run ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts;
acbuild ${debug} run git clone --config gcrypt.gpg-args="--batch --pinentry-mode loopback --passphrase ${passphrase}" gcrypt::${credentials};
acbuild ${debug} run chmod 600 credentials/ssh/id_rsa; #TODO fix this

# Retrieve states
echo -e "${Bold}>>${White} Retrieve state${Reset}";
acbuild ${debug} set-working-directory /home/${user}/salt/states
acbuild ${debug} run git clone git@gitlab.com:etnbrd/chloride.git;
acbuild ${debug} run git clone ${sodium};

# Apply minion configuration, machine grains, then states
echo -e "${Bold}>>${White} Apply states${Reset}";
acbuild ${debug} set-user root
acbuild ${debug} run ln -sf /home/${user}/salt/states/sodium/states/base/minion /etc/salt/minion;
acbuild ${debug} run ln -sf /home/${user}/salt/states/sodium/machines/${machine}.yml /etc/salt/grains;
acbuild ${debug} run salt-call --local state.highstate;

# Cleaning
echo -e "${Bold}>>${White} Cleaning${Reset}";
acbuild ${debug} run rm -rf /home/${user}/precred
acbuild ${debug} run yes | pacman -Scc; # Remove all package cache

# Finishing
acbuild ${debug} set-user ${user}
acbuild ${debug} set-working-directory /home/${user}
acbuild ${debug} write --overwrite ${name}.aci
