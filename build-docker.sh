#! /bin/sh

#
# Build a docker image with sodium/chloride configured as the machine given in argument
#

Bold='\033[1;32m';
Reset='\033[0m';

name=$1
user=$2
sodium=$3
credentials=$4

method='local'

echo -e "${Bold}>>${Reset} Building ${Bold}${user}${Reset}/${Bold}${name}${Reset}";

echo -n "Passphrase : ";
read -s passphrase;
echo;

docker build -t "${user}/${name}" \
  --no-cache \
  -f ~/.cellar/settler/Dockerfile.${method} \
  --build-arg passphrase="${passphrase}" \
  --build-arg rsa="`cat ~/.cellar/credentials/ssh/id_rsa`" \
  --build-arg pgp="`cat ~/.cellar/credentials/pgp/private.pgp`" \
  --build-arg machine=${name} \
  --build-arg user=${user} \
  --build-arg sodium=${sodium} \
  --build-arg credentials=${credentials} \
  .
