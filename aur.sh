#! /bin/sh

#
# Minimal AUR helper
#

cd /tmp
su -p nobody -s /bin/bash -c "curl https://aur.archlinux.org/cgit/aur.git/snapshot/${1}.tar.gz | tar xz"
cd ${1}
source ./PKGBUILD
if [ $(vercmp $pkgver $( pacman -Q ${1} | cut -d ' ' -f2 ) ) == 1 ]; then
  pacman -S --needed --noconfirm --asdeps ${depends[@]} ${makedepends[@]}
  su -p nobody -s /bin/bash -c "makepkg -c"
  pacman -U --needed --noconfirm *.pkg.tar.xz
else
  echo "nothing to do. $1 is up to date $pkgver $(pacman -Q $1)"
fi;
