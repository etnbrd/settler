#! /bin/sh

#
# push necessary files to a minimal archlinux setup, before deploying sodium / chloride
#

Bold='\033[1;32m';
White='\033[1;37m';
Reset='\033[0m';

echo -e "${Bold}>>${White} Pre-Deploying ${Bold}root${White}@${Bold}local${Reset}";

cp -r \
aur.sh \
deploy.sh \
git-remote-gcrypt.patch \
../credentials/ssh \
../credentials/pgp \
README.md \
/mnt/root
