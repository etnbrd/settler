#! /bin/sh

#
# Enter a docker container, based on its name and machine
# Create the container if it doesn't already exist
#

Bold='\033[1;32m';
Reset='\033[0m';

machine=$1
name=$2
dir=$3

echo -e "${Bold}>>${Reset} Entering ${Bold}${name}${Reset}"

# TODO fix this sudo thing
running=$(sudo docker inspect --format="{{.State.Running}}" ${name} 2> /dev/null)

if [ $? -eq 1 ]; then # When container does not yet exist
  echo -e "${Bold}>>${Reset} ${Bold}${name}${Reset} non existent, creating."
  docker run -it \
    --name $name \
    --net=host \
    -v /home/etn/salt/credentials:/home/etn/salt/credentials:rw \
    -v ${dir}:/home/etn/src:rw \
    $machine \
    /bin/zsh;
  exit 0;
fi

if [ "$running" == "false" ]; then # when container is stoped
  echo -e "${Bold}>>${Reset} ${Bold}${name}${Reset} stopped, restarting."
  docker start -ai $name;
  exit 0;
fi

if [ "$running" == "true" ]; then # when container is already running
  echo -e "${Bold}>>${Reset} ${Bold}${name}${Reset} started, new prompt."
  docker exec -it $name /bin/zsh;
  exit 0;
fi
