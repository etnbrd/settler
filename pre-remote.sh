#! /bin/sh

#
# push necessary files to a minimal archlinux setup, before deploying sodium / chloride
#

# example :
# ./pre-deploy.sh "host"

Bold='\033[1;32m';
White='\033[1;37m';
Reset='\033[0m';

host=$1
user=$2

echo -e "${Bold}>>${White} Pre-Deploying ${Bold}${user}${White}@${Bold}${host}${Reset}";

scp -r \
aur.sh \
deploy.sh \
git-remote-gcrypt.patch \
/home/${user}/salt/credentials/ssh \
/home/${user}/salt/credentials/pgp \
root@${1}:
